{
  'variables': {
    'owlfacts_path': 'owlfacts',
    'alchemy_path': 'alchemy',
    'cef_path': 'cef',
  },
  'target_defaults': {
    'conditions': [
      ['OS=="linux"', {'defines': ['__LINUX__']}],
      ['OS=="mac"', {
        'defines': ['__MAC__'],
        'xcode_settings': {
          'CLANG_CXX_LANGUAGE_STANDARD': 'c++0x',
          'CLANG_CXX_LIBRARY': 'libc++'
        }
      }],
      ['OS=="win"', {
        'defines': [
          '__WIN__',
          'UNICODE',
          'NOMINMAX',
        ]
      }],
    ],
    'cflags': [
      '-Wall',
      '-std=c++0x'
    ],
    'msbuild_toolset': 'v120_CTP_Nov2012',
    'configurations': {
      'Debug': {
        'defines': [
          'DEBUG',
        ],
        'msvs_settings': {
          'VCCLCompilerTool': {
            'Optimization': '0',
          },
          'VCLinkerTool': {
            'GenerateDebugInformation': 'true',
          },
        },
        'xcode_settings': {
          'GCC_GENERATE_DEBUGGING_SYMBOLS ': 'YES',
          'GCC_DEBUGGING_SYMBOLS': 'full',
          'DEBUG_INFORMATION_FORMAT': 'dwarf-with-dsym',
          'GCC_OPTIMIZATION_LEVEL': '0',
        },
      },
      'Release': {
        'defines': [
          'RELEASE',
        ],
      },
    },
  },
  'targets': [
    {
      # produces an executable on linux/windows, an app bundle on mac
      'target_name': 'demo',
      'type': 'executable',
      'mac_bundle': 1,
      'include_dirs': [
        '<(alchemy_path)',
      ],
      'sources': [
        '<(owlfacts_path)/src/main.cpp',
      ],
      'conditions': [
        ['OS=="linux"', {
          'libraries': [
            '<(alchemy_path)/alchemy.so',
            '<(cef_path)/libcef.so',
            '<(cef_path)/libcef_dll_wrapper.a'
          ],
          'copies': [
            {
              'destination': '<(PRODUCT_DIR)/locales',
              'files': ['<(cef_path)/locales/en-US.pak']
            },
            {
              'destination': '<(PRODUCT_DIR)',
              'files': ['<(cef_path)/chrome.pak']
            },
            {
              'destination': '<(PRODUCT_DIR)',
              'files': ['<(cef_path)/libcef.so']
            },
            {
              'destination': '<(PRODUCT_DIR)',
              'files': ['<(owlfacts_path)/ui']
            }
          ],
        }],
        ['OS=="mac"', {
          'libraries': [
            '$(SDKROOT)/System/Library/Frameworks/AppKit.framework',
            '<(alchemy_path)/alchemy.dylib',
            '<(cef_path)/libcef.dylib',
          ],
          'copies': [
            {
              'destination': '<(PRODUCT_DIR)/demo.app/Contents/MacOS',
              'files': ['<(cef_path)/libcef.dylib']
            },
            {
              'destination': '<(PRODUCT_DIR)/demo.app/Contents',
              'files': [
                '<(cef_path)/Resources',
                '<(owlfacts_path)/ui',
              ]
            }
          ],
        }],
        ['OS=="win"', {
          'libraries': [
            '<(alchemy_path)/alchemy.lib',
            '<(cef_path)/libcef_dll_wrapper.lib',
          ],
          'copies': [
            {
              'destination': '<(PRODUCT_DIR)/',
              'files': [
                '<(cef_path)/runtime/d3dcompiler_43.dll',
                '<(cef_path)/runtime/d3dx9_43.dll',
                '<(cef_path)/runtime/devtools_resources.pak',
                '<(cef_path)/runtime/icudt.dll',
                '<(cef_path)/runtime/libcef.dll',
                '<(cef_path)/runtime/libEGL.dll',
                '<(cef_path)/runtime/libGLESv2.dll',
                '<(cef_path)/libcef.lib',
              ]
            },
            {
              'destination': '<(PRODUCT_DIR)/locales',
              'files': ['<(cef_path)/runtime/locales/en-US.pak']
            },
            {
              'destination': '<(PRODUCT_DIR)/ui',
              'files': [
                '<(owlfacts_path)/ui/facts.html',
                '<(owlfacts_path)/ui/home.html',
                '<(owlfacts_path)/ui/index.html',
                '<(owlfacts_path)/ui/main.css',
                '<(owlfacts_path)/ui/page.css',
                '<(owlfacts_path)/ui/bg.jpg',
              ]
            },
          ],
        }],
      ],
    },
  ],
}
