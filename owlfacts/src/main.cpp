#include <iostream>
#include <string>
#include <thread>
#include <chrono>

#include "alchemy/alchemy.h"

using namespace std;

unique_ptr<Alchemy::Application> app;
vector<string> owlFacts;
int currentFact = 0;

struct Math {
  double add(int a, int b) {
    return a + b;
  }

  static int square(int num) {
    return num * num;
  }
};

string getUserName() {
  return "DenverCoder9";
}

string getRandomOwlFact() {
  currentFact %= owlFacts.size();
  return owlFacts[currentFact++];
}

void callFunction(const Alchemy::JS::Function& function) {
  cout << "Calling front-end function " << function.Name() << " from back-end" << endl;
  function(); //empty args will still work
  function("Hello, World!");
  function("Hello, World!", 1);
}

void testObject(const Alchemy::JS::Object& obj) {
  cout << "Inspecting object passed from front-end..." << endl;
  for (auto pair : obj.GetMap()) {
    cout << "  " << pair.first << " => " << pair.second.GetDebugString() << endl;
  }
}

void testArray(const Alchemy::JS::Array& obj) {
  cout << "Inspecting array passed from front-end..." << endl;
  auto vec = obj.GetVector();
  for (int i=0; i<vec.size(); i++) {
    cout << "  " << i << " => " << vec.at(i).GetDebugString() << endl;
  }
}

void addFacts() {
  owlFacts.push_back("FACT: There are about 205 species of owls.<br><br>Owls belong to a group of birds that includes about 205 species. These species are sorted into two basic groups, the barn owls and the true owls.<br><br>Barn owls have a heart-shaped face, long legs and powerful talons. Barn owls are medium-sized birds with a wingspan of about 3.5 feet. There are 16 species of barn owls, including the greater sooty owl, Australasian grass owl, ashy-faced owl, barn owl and Sulawesi owl.<br><br>True owls are more diverse than barn owls, with nearly 190 species in about 23 genera. Some better known true owls include screech owls, horned owls and saw-whet owls. True owls vary in size from the tiny elf owl to the bulky Eurasian eagle owl. True owls have a round facial disc, a short tail and a large head. Their color is muted (consisting of mostly brown, rust, gray, white and black) and their pattern is mottled, helping to conceal them from both predators and prey.");
  owlFacts.push_back("FACT: Owls are predators.<br><br>Owls feed on a wide variety of prey. Their main food source consists of small mammals such as mice, squirrels, voles and rabbits. They also supplement their diet by feeding on birds, insects and reptiles. Owls cannot chew their prey since, like all birds, they do not have teeth. Instead, they swallow small prey whole. They must tear larger prey into small pieces before swallowing. They later regurgitate pellets of indigestible material such as bone, fur and feathers.");
  owlFacts.push_back("FACT: Most owls are nocturnal.<br><br>Most owls hunt at night and in doing so avoid competition with daytime avian hunters such as hawks and eagles. Although nocturnal feeding is the norm for most owls, some species such as burrowing owls and short-eared owls feed during the day. Still other species, such as pygmy owls, feed at dusk or dawn.");
  owlFacts.push_back("FACT: Owl eyes are fixed in their sockets.<br><br>Owls are unable to move their eyes within their sockets to a great extent, which means they must turn their entire head to see in a different direction. Because owls have forward-facing eyes, they have well-developed binocular vision.");
  owlFacts.push_back("FACT: Many species of owls have special flight feathers adapted for silent flight.<br><br>Owls have developed special feather adaptations that enable them to minimize the sound made when flapping their wings. For instance, the leading edges of their primary feathers have a stiff fringes that reduces noise while the trailing edge of their primaries have soft fringes that helps to reduce turbulence. Downy feathers cover the surfaces of the wing to further reduce sound.");
  owlFacts.push_back("FACT: Owls have long been a part of human folklore and legend.<br><br>Owls are depicted in cave paintings in France that date back 15,000 to 20,000 years. Owls also appear in Egyptian hieroglyphics. They have held a variety of symbolic roles in culture and have represented misfortune, death, prosperity, and wisdom.");
  owlFacts.push_back("FACT: The tufts of feathers atop some owl's heads, referred to as 'ear tufts' are for display only.<br><br>Owls' ears are located on the facial disc behind the eyes and are concealed by feathers. Owls have an acute sense of hearing that helps them locate and capture prey. In some species, the ears are located asymmetrically on either side of the facial disc to enhance their ability to pinpoint the origin of the sounds they hear by sensing the minute difference in the time that sound reaches each ear.");
  owlFacts.push_back("FACT: Owls have strong feet like raptors, with two forward-facing toes and two backward-facing toes.<br><br>The structure of an owl's foot is referred to as zygodactyl, which means that tow of the toes face forward while two face backward. This arrangement enables the owls to capture and grasp prey with greater ease. Sometimes, the third toe can be rotated forward into a position occasionally used for perching.");
  owlFacts.push_back("FACT: Owls have a long, hooked bill.<br><br>In many species, the bill is partly concealed by feathers so it appears smaller than it actually is. Owls use their sharp bill to tear their food.");
  owlFacts.push_back("FACT: Owls do more than just hoot - they create many different vocalizations.<br><br>Owls create a wide variety of sounds or vocalizations. The familiar hoot is usually a territorial declaration, though not all species are able to hoot. Other sounds owls might make include screeches, hisses, and screams. Owl vocalizations are loud and low-pitched. Their cries travel well through the night air, enabling them to locate mates and declare territories despite the darkness.");
}

void threadTest() {
  auto homePage = app->Page("home.html");

  std::cout << "Waiting for " << homePage->GetURL() << " to load" << std::endl;
  while(!homePage->Ready()) {
    this_thread::sleep_for(chrono::milliseconds(100));
  }

  std::cout << "Executing home.html functions from non-UI thread." << std::endl;

  //function defined in main.cpp
  string u = homePage->Call("getUserName");
  std::cout << "getUserName returns: " << u << std::endl;

  //member function defined in main.cpp
  int add = homePage->Call("add", 2, 3);
  std::cout << "add returns: " << add << std::endl;

  //static member function defined in main.cpp
  int sqr = homePage->Call("square", 7);
  std::cout << "square returns: " << sqr << std::endl;

  //function defined in home.html
  int i = homePage->Call("subtract", 2, 3);
  std::cout << "subtract returns: " << i << std::endl;

  //function defined in home.html
  std::cout << "logType (outputs to console: int->number, double->number, bool->boolean, string->string)." << std::endl;
  homePage->Call("logType", 1000);
  homePage->Call("logType", 1.5);
  homePage->Call("logType", false);
  homePage->Call("logType", "test string");
}

int main() {
  Alchemy::Settings settings;
  settings.Directory("ui");
  settings.DefaultWindowSize(Alchemy::Size(800, 600));
  app = Alchemy::Application::Create(settings);
  
  std::thread t(threadTest);
  Math math;

  addFacts();

  auto home  = app->Page("home.html");
  auto facts = app->Page("facts.html");
  auto index = app->Page("index.html");

  home->Bind("getUserName", getUserName);
  home->Bind("add", &Math::add, math);
  home->Bind("square", &Math::square);
  home->Bind("callFunction", callFunction);
  home->Bind("testObject", testObject);
  home->Bind("testArray", testArray);
  facts->Bind("getRandomOwlFact", getRandomOwlFact);

  std::cout << "Owl Facts - About to LoadPage" << std::endl; //remove soon
  
  app->Show(index);

  t.join();

  return 0;
}